1. Apakah perbedaan antara JSON dan XML?

   JSON adalah format yang di tulis dalam javascript sedangkan XML adalah bahasa markup yang memiliki tag untuk mendefinisikan elemen. JSON lebih mudah dibaca daripada XML tetapi untuk pengolahan data yang besar dan komplek XML jauh lebih baik dari JSON. XML mendukung namespaces dan komentar sedangkan JSON tidak mendukung. JSON mendukung banyak tipe data seperti string, array, boolean, dan angkaa sedangkan XML hanya mendukung tipe data yang kompleks. JSON hanya mendukung UTF-8 sedangkan XML mendukung beberapa macam UTF. Untuk tingkat keamanan JSON lebih aman dibandingkan XML. Untuk browser JSON didukung hampir diseluruh broser sedangkan XML hanya didukung oleh beberapa browser atau lebih terbatas.

2. Apakah perbedaan antara HTML dan XML?

   HTML adalah bahasa markup standar untuk membuat halaman web dan aplikasi web sedangkan XML adalah bahasa markup yang digunakan untuk transfer data yang dapat dibaca mesin. XML case sensitive sedangkan HTML tidak case sensitive. XML harus menggunakan tag penutup sedangkan HTML tag penutup dapat digunakan atau tidak. XML menyediakan dukungan namespaces sementara HTML tidak menyediakan dukungan namespaces.
