from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['to', 'From', 'title', 'message']

    to = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': "form-control"}))
    From = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': "form-control"}))
    title = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': "form-control"}))
    message = forms.CharField(widget=forms.Textarea(attrs={'class': "form-control"}))