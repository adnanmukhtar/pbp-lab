import 'package:flutter/material.dart';
import 'package:lab_7/screen/form.dart';

class BottomNavigation extends StatefulWidget {
  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  int _currentTab = 2;
  final List<Widget> _screen = [
    Center(
      child: Text("Home", style: TextStyle(fontSize: 18)),
    ),
    Center(
      child: Text("Donasi", style: TextStyle(fontSize: 18)),
    ),
    FormScreen(),
  ];

  void _onTapped(int index) {
    setState(() {
      _currentTab = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // centerTitle: true,
        backgroundColor: Colors.white,
        title: RichText(
          text: TextSpan(children: <TextSpan>[
            TextSpan(
                text: "Konva", style: TextStyle(fontSize: 26, color: Colors.red)),
            TextSpan(
                text: "Search", style: TextStyle(fontSize: 26, color: Colors.redAccent))
          ]),
        ),
      ),
      backgroundColor: Color.fromRGBO(220, 234, 249, 1),
      body: _screen.elementAt(_currentTab),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentTab,
        selectedItemColor: Colors.red,
        onTap: _onTapped,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home_filled),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.health_and_safety_rounded),
            label: "Donasi",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search_rounded),
            label: "Cari Donasi",
          ),
        ],
      ),
    );
  }
}
