import 'package:flutter/material.dart';

class FormScreen extends StatefulWidget {
  const FormScreen({Key? key}) : super(key: key);

  @override
  _FormScreenState createState() => _FormScreenState();
}

class _FormScreenState extends State<FormScreen> {
  var _formKey = GlobalKey<FormState>();

  void _submit() {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      _formKey.currentState!.save();
      print("Nama lengkap: "+_nama!);
      print("NIK: "+_nik!);
      print("Nomor HP: "+_nomorHp!);
      print("Jenis Kelamin: "+_gender);
      print("Tempat lahir: "+_tempatLahir!);
      print("Tanggal lahir: "+_tanggalLahir!);
      print("Alamat: "+_alamat!);
      print("Golongan darah: "+_goldar);
      print("Rhesus: "+_rhesus);
      print("Nama rumah sakit: "+_namaRumahSakit!);
      print("Urgency: "+_urgency);
    }
  }

  String? _nama;
  String? _nik;
  String? _nomorHp;
  String? _tempatLahir;
  String? _tanggalLahir;
  String? _alamat;
  String? _namaRumahSakit;
  String _gender = "Laki-Laki";
  List _listGender = ["Laki-Laki", "Perempuan"];

  String _goldar = "A";
  List _listGoldar = [
    "A",
    "B",
    "AB",
    "O",
  ];

  String _rhesus = "+";
  List _listRhesus = [
    "+",
    "-",
  ];

  String _urgency = "Low";
  List _listUrgency = [
    "Low",
    "Medium",
    "High",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(220, 234, 249, 1),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.width * 0.05),
              Text(
                "Form Pencari Donor",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: MediaQuery.of(context).size.width * 0.05),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: TextFormField(
                  decoration: InputDecoration(
                    // border: OutlineInputBorder(),
                    labelText: 'Nama Lengkap',
                    labelStyle: TextStyle(fontWeight: FontWeight.bold),
                    // icon: Icon(Icons.people)
                  ),
                  // onFieldSubmitted: (value) {},
                  onSaved: (String? value) {
                    _nama = value;
                  },
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Nama Tidak boleh kosong';
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.width * 0.05),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: TextFormField(
                  decoration: InputDecoration(
                      // border: OutlineInputBorder(),
                      labelText: 'NIK',
                      labelStyle: TextStyle(fontWeight: FontWeight.bold)),
                  keyboardType: TextInputType.number,
                  // onFieldSubmitted: (value) {},
                  onSaved: (String? value) {
                    _nik = value;
                  },
                  validator: (value) {
                    if (value!.length < 16  || !RegExp(r'^[0-9]+$').hasMatch(value)) {
                      return 'NIK harus terdiri dari 16 angka';
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.width * 0.05),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: TextFormField(
                  decoration: InputDecoration(
                    // border: OutlineInputBorder(),
                    labelText: 'Nomor HP',
                    labelStyle: TextStyle(fontWeight: FontWeight.bold),
                    // icon: Icon(Icons.phone)
                  ),
                  keyboardType: TextInputType.phone,
                  // onFieldSubmitted: (value) {},
                  onSaved: (String? value) {
                    _nomorHp = value;
                  },
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Nomor HP tidak boleh kosong';
                    }else if(!RegExp(r'^[0-9]+$').hasMatch(value)){
                      return 'Input harus angka';
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.width * 0.05),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Jenis Kelamin",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15)),
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Container(
                        width: 300,
                        child: DropdownButton<String>(
                          // icon: Icon(Icons.wc_rounded),
                          value: _gender,
                          items: _listGender.map((value) {
                            return DropdownMenuItem<String>(
                              child: Text(value),
                              value: value,
                            );
                          }).toList(),
                          onChanged: (String? value) {
                            setState(() {
                              _gender = value!;
                            });
                          },
                        ),
                      ),
                    ]),
                  ],
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.width * 0.05),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: TextFormField(
                  decoration: InputDecoration(
                      // border: OutlineInputBorder(),
                      labelText: 'Tempat Lahir',
                      labelStyle: TextStyle(fontWeight: FontWeight.bold)),
                  // onFieldSubmitted: (value) {},
                  onSaved: (String? value) {
                    _tempatLahir = value;
                  },
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Tempat Lahir tidak boleh kosong';
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.width * 0.05),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: TextFormField(
                  decoration: InputDecoration(
                      // border: OutlineInputBorder(),
                      labelText: 'Tanggal Lahir',
                      labelStyle: TextStyle(fontWeight: FontWeight.bold),
                      hintText: "dd/mm/yyyy"),
                  // onFieldSubmitted: (value) {},
                  onSaved: (String? value) {
                    _tanggalLahir = value;
                  },
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Tanggal Lahir tidak boleh kosong';
                    }
                    return null;
                  },
                ),
              ),
              // SizedBox(height: MediaQuery.of(context).size.width * 0.05),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: TextFormField(
                  decoration: InputDecoration(
                      // border: OutlineInputBorder(),
                      labelText: 'Alamat',
                      labelStyle: TextStyle(fontWeight: FontWeight.bold)),
                  keyboardType: TextInputType.multiline,
                  minLines: 6,
                  maxLines: null,
                  // onFieldSubmitted: (value) {},
                  onSaved: (String? value) {
                    _alamat = value;
                  },
                  // obscureText: true,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Alamat tidak boleh kosong';
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.width * 0.05),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Golongan Darah",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15)),
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Container(
                        width: 300,
                        child: DropdownButton<String>(
                          value: _goldar,
                          items: _listGoldar.map((value) {
                            return DropdownMenuItem<String>(
                              child: Text(value),
                              value: value,
                            );
                          }).toList(),
                          onChanged: (String? value) {
                            setState(() {
                              _goldar = value!;
                            });
                          },
                        ),
                      ),
                    ]),
                  ],
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.width * 0.05),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Rhesus",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15)),
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Container(
                        width: 300,
                        child: DropdownButton<String>(
                          value: _rhesus,
                          items: _listRhesus.map((value) {
                            return DropdownMenuItem<String>(
                              child: Text(value),
                              value: value,
                            );
                          }).toList(),
                          onChanged: (String? value) {
                            setState(() {
                              _rhesus = value!;
                            });
                          },
                        ),
                      ),
                    ]),
                  ],
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.width * 0.05),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: TextFormField(
                  decoration: InputDecoration(
                      // border: OutlineInputBorder(),
                      labelText: 'Nama Rumah Sakit Tempat anda dirawat',
                      labelStyle: TextStyle(fontWeight: FontWeight.bold)),
                  // onFieldSubmitted: (value) {},
                  onSaved: (String? value) {
                    _namaRumahSakit = value;
                  },
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Nama rumah sakit tidak boleh kosong';
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.width * 0.05),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Urgency",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15)),
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Container(
                        width: 300,
                        child: DropdownButton<String>(
                          value: _urgency,
                          items: _listUrgency.map((value) {
                            return DropdownMenuItem<String>(
                              child: Text(value),
                              value: value,
                            );
                          }).toList(),
                          onChanged: (String? value) {
                            setState(() {
                              _urgency = value!;
                            });
                          },
                        ),
                      ),
                    ]),
                  ],
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.width * 0.05),
              Container(
                width: 250,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blue,
                  ),
                  child: Text(
                    "Submit",
                    style: TextStyle(fontSize: 20.0, color: Colors.white),
                  ),
                  onPressed: () {
                    _submit();
                  },
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.width * 0.05),
            ],
          ),
        ),
      ),
    );
  }
}
