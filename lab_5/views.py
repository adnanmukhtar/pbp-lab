from django.http.response import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from lab_4.forms import NoteForm
from lab_2.models import Note


# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

def view_note(request, id):
    notes = Note.objects.get(id=id)
    response = {'note' : notes}
    return render(request, "lab5_view.html", response)

def delete_note(request, id):
    notes = Note.objects.get(id=id)
    notes.delete()
    return HttpResponseRedirect('/lab-5')

def update_note(request, id):
    notes = Note.objects.get(id=id)
    form = NoteForm(request.POST or None, instance=notes)
    if request.method == 'POST':
        form.save()
        return HttpResponseRedirect('/lab-5')

    response = {'form' : form}
    return render (request, 'lab5_update.html', response)


