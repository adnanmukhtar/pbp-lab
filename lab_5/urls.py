from django.urls import path
from .views import index, view_note, delete_note, update_note

urlpatterns = [
    path('', index, name='index'),
    path('notes/<id>', view_note),
    path('notes/<id>/update', update_note),
    path('notes/<id>/delete', delete_note),
]